#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
from PyQt4 import QtGui
from PyQt4 import QtCore
from PyQt4 import *
from mainwindowdlg import MainWindowDlg
from client import client

app = QtGui.QApplication(sys.argv)
dialog = MainWindowDlg()
myclient = client()

QtCore.QObject.connect(dialog,QtCore.SIGNAL("connectJabber()"),myclient.enableJabber)

dialog.show()
sys.exit(app.exec_())

