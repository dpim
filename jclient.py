import sys,xmpp


class jclient():
    def __init__(self,userjid,password):
        self.userjid = userjid
        self.password = password
    def connect(self):
        print "connecting"

        jid=xmpp.protocol.JID(self.userjid)
        self.cl=xmpp.Client(jid.getDomain(),debug=[])
        con=self.cl.connect()

        if not con:
            print 'could not connect!'
            sys.exit()
        print 'connected with',con


        auth=self.cl.auth(jid.getNode(),self.password,resource=jid.getResource())
        if not auth:
            print 'could not authenticate!'
            sys.exit()
        print 'authenticated using',auth

        #cl.SendInitPresence(requestRoster=0)   # you may need to uncomment this for old server
      
    def sendMessage(self,to_jid,text):
        id=self.cl.send(xmpp.protocol.Message(to_jid,text))
        print 'sent message with id',id

    def disconnect(self):
        self.cl.disconnect()
