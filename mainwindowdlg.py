#!/usr/bin/python
# -*- coding: utf-8 -*-

from ui_mainwindowdlg import Ui_MainWindowDlg 
from PyQt4 import QtGui,QtCore


class MainWindowDlg(QtGui.QMainWindow, Ui_MainWindowDlg):
    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        self.setupUi(self)
        QtCore.QObject.connect(self.SendButton,QtCore.SIGNAL("clicked()"),self.onSendButtonClicked)

    def connectJabber(self):
        self.emit (QtCore.SIGNAL("connectJabber()"))
    def onSendButtonClicked(self):
        msg = self.InputLine.text()
        if len(msg) < 1:
            return False
        self.ChatView.insertPlainText(msg+"\n")